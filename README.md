# Atack on monoalphabetic cipher method
> program show how to use statistic to break monoalphabetic cipher method

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Project Status](#project-status)
* [Contact](#contact)


## General Information
- This project was realized on IV semester of studies.
- Program use statistic to break monoalphabetic cipher method in two language.
- The purpose of project was to learn Python and cryptography methods.


## Technologies Used
- Python - version 3.11
- PyCharm Community Edition
- Statistic of the frequency of letters in a language


## Features
Provided features:
- Make a statistic about letters
- the final program have a key
- Description show way how the key was get



## Project Status
Project is: _no longer being worked on_. 


## Contact
Created by Piotr Hajduk

