

try:
    namess = int(input("Wybierz \n1 dla tekst 1\n2 dla tekstu 2\n"))
    file_name = ''
    if namess == 1:
        file_name = "tekst1.txt"
    elif namess == 2:
        file_name = "tekst2.txt"

    with open(file_name, 'r') as file:
        text = file.read()
        file.close()


    def procent_letters(text):
        dlugsc_textu = len(text)
        alphabet = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0, 'G': 0, 'H': 0, 'I': 0, 'J': 0, 'K': 0, 'L': 0, 'M': 0,
                    'N': 0, 'O': 0, 'P': 0, 'Q': 0, 'R': 0, 'S': 0, 'T': 0, 'U': 0, 'V': 0, 'W': 0, 'X': 0, 'Y': 0, 'Z': 0}
        for letter in text:
            alphabet[letter] += 1
        for key, value in alphabet.items():
            temp = alphabet[key]
            x = round(((temp / dlugsc_textu) * 100), 3)
            alphabet[key] = x
        t = sorted(alphabet.items(), key=lambda x: x[1], reverse=True)
        return dict(t)


    def create_key(x, y):
        for k in x.keys():
            temp.append(k)
        for t in y.keys():
            temp2.append(t)
        return dict(zip(temp, temp2))


    def text_format(text):
        res = ''
        for i in range(len(text)):
            res = res + text[i]
            if (i + 1) % 5 == 0:
                res = res + ' '
            if (i + 1) % 35 == 0:
                res = res + '\n'
        return res


    #tekst1 = procent_letters(text)
    staty_polskie = {'a': 8,
                     'b': 1.3,
                     'c': 3.8,
                     'd': 3,
                     'e': 6.9,
                     'f': 0.1,
                     'g': 1,
                     'h': 1,
                     'i': 7,
                     'j': 1.9,
                     'k': 2.7,
                     'l': 3.1,
                     'm': 2.4,
                     'n': 4.7,
                     'o': 7.1,
                     'p': 2.4,
                     'q': 0,
                     'r': 3.5,
                     's': 3.8,
                     't': 2.4,
                     'u': 1.8,
                     'v': 0,
                     'w': 3.6,
                     'x': 0,
                     'y': 3.2,
                     'z': 5.8
                     }
    staty_eng = {'a': 8.2,
                 'b': 1.5,
                 'c': 2.8,
                 'd': 4.3,
                 'e': 12.7,
                 'f': 2.2,
                 'g': 2,
                 'h': 6.1,
                 'i': 7,
                 'j': 0.2,
                 'k': 0.8,
                 'l': 4,
                 'm': 2.4,
                 'n': 6.7,
                 'o': 7.5,
                 'p': 1.9,
                 'q': 0.1,
                 'r': 6,
                 's': 6.3,
                 't': 9.1,
                 'u': 2.8,
                 'v': 1,
                 'w': 2.3,
                 'x': 0.1,
                 'y': 2,
                 'z': 0.1
                 }

    key_pl = dict(sorted(staty_polskie.items(), key=lambda x: x[1], reverse=True))
    key_eng = dict(sorted(staty_eng.items(), key=lambda x: x[1], reverse=True))

    temp = []
    temp2 = []

    # slownik = dict(zip(temp,temp2))

    #key_teskt1_stat = create_key(tekst1, key_pl)
    #print(key_teskt1_stat)

    correct_key_ang = {'A': 'e',  # git
                       'M': 't',  # git
                       'O': 'n',  # git
                       'V': 'a',  # git
                       'Y': 'r',  # git
                       'D': 'h',  # git
                       'C': 'd',  # git
                       'H': 's',
                       'J': 'v',
                       'I': 'c',
                       'E': 'u',
                       'W': 'i',
                       'X': 'o',
                       'U': 'g',
                       'G': 'p',
                       'N': 'l',
                       'B': 'w',
                       'Q': 'b',
                       'F': 'y',
                       'S': 'f',
                       'K': 'm',
                       'T': 'q',
                       'P': 'x',
                       'L': 'z',
                       'R': 'j',
                       'Z': 'k'
                       }  # Klucz do tekstu 2
    correct_klucz = {
        'T': 'a',
        'C': 'i',
        'R': 'e',
        'B': 'z',
        'H': 'n',
        'Q': 'w',
        'I': 'y',
        'Y': 'k',
        'F': 'd',
        'G': 'l',
        'S': 'f',
        'N': 'p',
        'W': 'r',
        'M': 'o',
        'L': 'u',
        'O': 'c',
        'D': 't',
        'A': 's',
        'X': 'j',
        'V': 'h',
        'Z': 'm',
        'K': 'g',
        'P': 'b',
    }  # klucz do tekstu 1

    if namess == 1:
        key_deszyfr = correct_klucz
        result_name = 'result_polski.txt'
    elif namess == 2:
        key_deszyfr = correct_key_ang
        result_name = 'result_english.txt'
    else:
        result_name = 0
        key_deszyfr = {}
    u = []
    result = ''
    for i in text:
        u.append(i)
    for j in range(len(u)):
        if u[j] in key_deszyfr.keys():
            for key, value in key_deszyfr.items():
                if u[j] == key:
                    result = result + value
        else:
            result += u[j]

    file = open(result_name, 'w')
    file.write(text_format(result))
    file.close()
    print(f"Wynik deszyfrowania zapisany w pliku {result_name}\n")
    # Sprawdzanie paru znakow
    # alphabet_2 = {'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 0, 'f': 0, 'g': 0, 'h': 0, 'i': 0, 'j': 0, 'k': 0, 'l': 0, 'm': 0,
    #              'n': 0, 'o': 0, 'p': 0, 'q': 0, 'r': 0, 's': 0, 't': 0, 'u': 0, 'v': 0, 'w': 0, 'x': 0, 'y': 0, 'z': 0}
    # for i in range(len(result) - 3):
    #   if result[i] == 'i' and result[i + 1] == 't':
    #      alphabet_2[result[i + 2]] += 1
    # print(alphabet_2)
except:
    print("chuj")